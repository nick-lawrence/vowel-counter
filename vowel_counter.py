runme = True
while runme == True:
  sentance = input("Type your sentance here [type 'quit' to exit]: ")
  vowels = frozenset("aeiou")
  vcount = []

  if sentance == 'quit':
    break

  for letter in sentance:
    if letter in vowels:
      vcount.append(letter)

  if vcount:
    print(f"There were {len(vcount)} vowels in your sentance.")
  else:
    print("No vowels in sentance")

